Install node
===========================
$ sudo apt-get install nodejs

Install npm
===========================
$ sudo apt-get install npm

Install npm packages
===========================
$ npm install

Run on production | stop
===========================
$ ./start.sh
$ ./stop.sh

Config start.sh
===========================
#!/bin/bash

./node_modules/forever/bin/forever \
start \
-al forever.log \
-ao out.log \
-ae err.log \
app.js

Config stop.sh
===========================
#!/bin/bash

# Invoke the Forever module (to STOP our Node.js server).
./node_modules/forever/bin/forever stop app.js
