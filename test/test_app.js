'use strict';

var app = require('../app.js'),
  supertest = require('supertest');

describe('App Routes', function() {
  it('/', function(done){
    supertest(app)
      .get('/')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(302, done);
  });

  it('/repos', function(done){
    this.timeout(15000);

    supertest(app)
      .get('/repos')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(200, /netflix/)
      .expect(200, done);
  });

  it('/repos/astyanax', function(done){
    this.timeout(15000);

    supertest(app)
      .get('/repos/astyanax/310/745')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(200, /astyanax/)
      .expect(200, done);
  });

  it('/repos/astyanaxforceerror/310/745 returns error', function(done){
    this.timeout(15000);

    supertest(app)
      .get('/repos/astyanaxforceerror/310/745')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(500, done);
  });

  it('/commits/astyanax', function(done){
    this.timeout(15000);

    supertest(app)
      .get('/commits/astyanax/1')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(200, /astyanax/)
      .expect(200, done);
  });

  it('/commits/astyanaxforceerror returns error', function(done){
    this.timeout(15000);

    supertest(app)
      .get('/commits/astyanaxforceerror/1')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(500, done);
  });

  it('/500', function(done){
    supertest(app)
      .get('/500')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(500, done);
  });

  it('/404', function(done){
    supertest(app)
      .get('/404')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(404, done);
  });

  it('/anything', function(done){
    supertest(app)
      .get('/anything')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(302, done);
  });

  it('/forceerror', function(done){
    supertest(app)
      .get('/forceerror')
      .set('Accept', 'text/html')
      .expect('Content-Type', 'text/html; charset=utf-8')
      .expect(500, done);
  });
});
