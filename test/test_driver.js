'use strict';

var webdriver = require('selenium-webdriver'),
  By = require('selenium-webdriver').By,
  until = require('selenium-webdriver').until,
  driver = new webdriver.Builder()
  .forBrowser('firefox')
  .usingServer('http://localhost:4444/wd/hub')
  .build();

driver.get('http://localhost:8002');
driver.wait(until.titleIs('Rastreabilidade'), 1000);
driver.findElement(By.name('astyanax')).click();
driver.wait(until.elementLocated(By.name('mdo'), 2000));
driver.quit();
