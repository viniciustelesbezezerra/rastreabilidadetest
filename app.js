'use strict';

var express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  queryString = require('query-string'),
  repos = require('./lib/repos.js'),
  app = module.exports = express();

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.use(methodOverride());
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('views', __dirname + '/public');
app.use('', express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.redirect('/repos');
});

app.get('/repos', repos.index);
app.get('/repos/:name/:forks/:stars', repos.show);
app.get('/commits/:repo/:page', repos.commits);

app.get('/forceerror', function(req, res) {
  res.status(500).send(variableerror);
});

app.get('/500', function(req, res) {
  res.status(500).render('500');
});

app.get('/404', function(req, res) {
  res.status(404).render('404');
});

app.get('*', function(req, res) {
  res.status(404).redirect('/404');
});

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.render('500');
});

app.listen(process.env.PORT || 8002);
