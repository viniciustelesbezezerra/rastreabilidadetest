'use strict';

var GithubRequest = require('./github_request.js'),
  GITHUB_API_ROOT = 'https://api.github.com';

exports.index = function(req ,res) {
  new GithubRequest({
    url: GITHUB_API_ROOT + '/orgs/netflix/repos?per_page=80',
    template: 'index',
    res: res
  });
};

exports.show = function(req ,res) {
  new GithubRequest({
    url: GITHUB_API_ROOT + '/repos/netflix/' +
      req.param('name') + '/commits?per_page=20',
    template: 'show',
    context: req,
    res: res
  });
};

exports.commits = function(req ,res) {
  new GithubRequest({
    url: GITHUB_API_ROOT + '/repos/netflix/' +
      req.param('repo') + '/commits?per_page=20&page=' +
      req.param('page'),
    template: 'commits',
    context: req,
    res: res
  });
};
