'use strict';

var superagent = require('superagent'),
    _ = require('underscore'),
    parse = require('parse-link-header');

var GitHubRequest = (function() {
  function GitHubRequest(params) {
    this.res = params.res,
    this.url = params.url,
    this.template = params.template,
    this.context = params.context;

    this.call();
  };

  GitHubRequest.prototype.call = function() {
    var self = this;

    superagent
      .get(this.url)
      .set('User-Agent', 'viniciustelesbezerra')
      .set('Accept', 'application/json')
      .end(function(error, response) {
        if (!error && response.statusCode == 200) {
          self.res.render(self.template, {
            objects: self.sortResult(response.body),
            next_page: self.getNextPage(response.headers.link),
            repo: self.getParam(self, 'repo'),
            name: self.getParam(self, 'name'),
            stars: self.getParam(self, 'stars'),
            forks: self.getParam(self, 'forks')
          });
        }
        else
          self.res.status(500).render('500');
      });
  };

  GitHubRequest.prototype.getParam = function(self, param) {
    try { return self.context.param(param); }
    catch(e) { return ''; }
  };

  GitHubRequest.prototype.sortResult = function(body) {
    return _.sortBy(body, 'stargazers_count').reverse();
  };

  GitHubRequest.prototype.getNextPage = function(link) {
    try { return parse(link).next.page; }
    catch(e) { return ''; }
  };

  return GitHubRequest;
})();

module.exports = GitHubRequest;
