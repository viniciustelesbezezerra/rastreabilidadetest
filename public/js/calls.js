'use strict';

var appendPage = function(data) {
  $('div.fetch').html(data);
};

var appendCommit = function(data) {
  $('a.commitlink').remove();
  $('div.commits').append(data);
};

var fetchPage = function(link, param) {
  $.get(link.data('url'), function(data, status) {

    if (param.repo)
      appendPage(data);
    else
      appendCommit(data);
  });
};

$(function() {
  $(document).on('click', 'a.repolink', function (event) {
    fetchPage($(this), {repo: true});
  });

  $(document).on('click', 'a.commitlink', function (event) {
    fetchPage($(this), {commit: true});
  });
});
